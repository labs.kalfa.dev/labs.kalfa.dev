.. title: About me
.. slug: about-me
.. date: 2020-09-01 15:05:43+01:00
.. tags: about
.. category: 
.. link: 
.. description: 
.. type: text

About
-----


I am Cosimo Alfarano, `software engineer <https://www.linkedin.com/in/kalfa/>`__ at `Agile Analog <https://www.linkedin.com/company/agile-analog/>`__

My personal page is http://cosimo.alfarano.net, while some more technical articles can be found at https://blog.kalfa.dev
